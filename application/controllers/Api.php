<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Kevin Chael Calma | kchael.10.26@gmail.com 
Class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('OrganizationModel');
	}

	public function index() {
		header('HTTP/1.0 403 Forbidden');
		echo 'Forbidden access';
	}
	// 1. Create an endpoint that shows the organization structure.
	// /api/organization
	public function organization(){

		$response = array();
		$employeeList = $this->OrganizationModel->employeeList(); // get all employees data
		// echo "<pre>";print_r($employeeList);die;
		foreach ($employeeList as $employees) {
			if ((int)$employees['employeeUnder'] > 0) {

				$employees['employeeUnder'] = $this->OrganizationModel->reportingTo($employees['employeeNumber']); // get employees data where employee is reporting to.

			} else {

				$employees['employeeUnder'] = array();

			} // end if employee under > 0

			$response[] = $employees;
		} // end of for each
		// echo "<pre>";print_r($response);die;
		echo json_encode($response, true);
	} // end of organization function

	// 2. Create an endpoint that shows each offices contains which employee.
	// /api/offices
	public function offices(){

		$response = array();
		$officeList = $this->OrganizationModel->officeList(); // get list of all officess

		foreach ($officeList as $office) {
			$office['employees'][] = $this->OrganizationModel->officeEmployees($office['officeCode']); // get employees data under officess
			$response[] = $office;

		} // end of for each
		// echo "<pre>";print_r($response);die;
		echo json_encode($response, true);
	} // end of offices function

	// 3. Create an endpoint for dashboard report where it shows total number of sales revenue of each product lines being shipped by each employee.
	public function sales_report($employeeNumber = 0){
		$response = array();
		// list of employees with total sales and commision
		$employeeList = $this->OrganizationModel->employeeSalesAndCommision($employeeNumber);
		foreach ($employeeList as $employees) {

			// employess product lines
			$employeeProductLine = $this->OrganizationModel->employeeProductLine($employees['employeeNumber']);
			foreach($employeeProductLine as $productLine){

				// product per product line
				$products = $this->OrganizationModel->productLinesProductSales($employees['employeeNumber'],$productLine['productLines']);
				foreach($products as $product){
					$productLine['products'][] = $product;
				} // end of products for each

				$employees['productLines'][] = $productLine;
			} // end of employee product line for each


			$response[] = $employees;
		} // end of employee list for each

		echo json_encode($response, true);
	} // end of sales_report function

}
