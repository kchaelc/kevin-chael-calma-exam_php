<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Kevin Chael Calma | kchael.10.26@gmail.com 
class OrganizationModel extends CI_Model { 

	public function employeeList() {

		$this->db->select("
		employeeNumber, Concat(firstName,' ',lastName) name, jobTitle, reportsTo employeeUnder
		");
		$this->db->from("employees");
		$results = $this->db->get()->result_array();
		
		return(empty($results) ? array() : $results);
	}

	public function reportingTo($employeeNumber = null) {
		
		$this->db->select("
		employeeNumber, Concat(firstName,' ',lastName) name, jobTitle, reportsTo employeeUnder
		");
		$this->db->from("employees");
		$this->db->where("employeeNumber",$employeeNumber);
		$results = $this->db->get()->row_array();
		
		return(empty($results) ? array() : $results);
	}

	public function officeList(){
		
		$this->db->select("
		officeCode, city
		");
		$this->db->from("offices");
		$results = $this->db->get()->result_array();

		return (empty($results) ? array() : $results);
	}

	public function officeEmployees($officeCode = null){
	
		$this->db->select("
		employeeNumber, Concat(firstName,' ',lastName) name, jobTitle
		");
		$this->db->from("employees");
		$this->db->where("officeCode",$officeCode);
		$results = $this->db->get()->result_array();

		return (empty($results) ? array() : $results);
	}

	public function employeeSalesAndCommision($employeeNumber = 0){
		
		// orderdetails a
		// orders b
		// products c
		// customers d
		// employees e
		// offices f

		$this->db->select("
		e.employeeNumber, Concat(e.firstName,' ',e.lastName) name, e.jobTitle, e.officeCode,
		f.city, FORMAT(SUM((a.quantityOrdered / SUBSTRING_INDEX(c.productScale, ':', -1))* a.priceEach), 2) totalCommission,
		SUM(a.quantityOrdered * a.priceEach) totalSales
		");
		$this->db->from("orderdetails a");
		$this->db->join("orders b","b.orderNumber = a.orderNumber","left");
		$this->db->join("products c","a.productCode = c.productCode","left");
		$this->db->join("customers d","d.customerNumber = b.customerNumber","left");
		$this->db->join("employees e","e.employeeNumber = d.salesRepEmployeeNumber","left");
		$this->db->join("offices f","e.officeCode = f.officeCode","left");
		$this->db->where("b.status",'Shipped');
		if ((int)$employeeNumber > 0) {
			$this->db->where("employeeNumber",$employeeNumber);
		}
		$this->db->group_by("e.employeeNumber");

		$results = $this->db->get()->result_array();

		return (empty($results) ? array() : $results);
	}

	public function employeeProductLine($employeeNumber = 0){
		
		// orderdetails a
		// orders b
		// products c
		// productLine d
		// customers e
		// employees f
		
		$this->db->select("
		c.productLine productLines, d.textDescription,
		round(sum((a.quantityOrdered / substring_index(c.productScale, ':', -1))* a.priceEach),2) commission,
		sum(a.quantityOrdered) quantity, sum(a.quantityOrdered * a.priceEach) sales
		");
		$this->db->from("orderdetails a");
		$this->db->join("orders b","b.orderNumber = a.orderNumber","left");
		$this->db->join("products c","a.productCode = c.productCode","left");
		$this->db->join("productlines d","c.productLine = d.productLine","left");
		$this->db->join("customers e","e.customerNumber = b.customerNumber","left");
		$this->db->join("employees f","f.employeeNumber = e.salesRepEmployeeNumber","left");
		$this->db->where("b.status",'Shipped');
		$this->db->where("f.employeeNumber",(int)$employeeNumber);
		$this->db->group_by("c.productLine");
		$results = $this->db->get()->result_array();

		return (empty($results) ? array() : $results);
		
	}

	public function productLinesProductSales($employeeNumber = 0, $productLine = ''){

		// orderdetails a
		// orders b
		// products c
		// productLine d
		// customers e
		// employees f
		
		$this->db->select("
		c.productCode, c.productName,
		sum(a.quantityOrdered) quantity, sum(a.quantityOrdered * a.priceEach) sales,
		count(e.customerNumber) as numberOfCustomerBought
		");
		$this->db->from("orderdetails a");
		$this->db->join("orders b","b.orderNumber = a.orderNumber","left");
		$this->db->join("products c","a.productCode = c.productCode","left");
		$this->db->join("productlines d","c.productLine = d.productLine","left");
		$this->db->join("customers e","e.customerNumber = b.customerNumber","left");
		$this->db->join("employees f","f.employeeNumber = e.salesRepEmployeeNumber","left");
		$this->db->where("b.status",'Shipped');
		$this->db->where("f.employeeNumber",(int)$employeeNumber);
		$this->db->where("c.productLine",$productLine);

		$results = $this->db->get()->result_array();

		return (empty($results) ? array() : $results);
	}

}